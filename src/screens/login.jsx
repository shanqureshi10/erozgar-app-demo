import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from "react-native";
import React from "react";
import { Ionicons } from "@expo/vector-icons";

const Login = () => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Ionicons name="chevron-back" size={33} />
        <Text style={styles.headerText}>Login</Text>
      </View>
      <View style={styles.form}>
        <TextInput style={styles.input} placeholder="email" />
        <TextInput style={styles.input} placeholder="password" />

        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.blank}></View>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 3,
    backgroundColor: "#fff8ff",
  },
  header: {
    flex: 0.5,
    justifyContent: "space-between",
    paddingHorizontal: 10,
    paddingVertical: 20,
    marginTop: 10,
  },
  headerText: {
    fontSize: 40,
    fontWeight: "bold",
  },
  form: {
    flex: 1.5,
    padding: 10,
  },
  input: {
    backgroundColor: "white",
    padding: 15,
    marginVertical: 10,
  },
  button: {
    width: "90%",
    padding: 10,
    backgroundColor: "red",
    borderRadius: 20,
    alignItems: "center",
    alignSelf: "center",
  },
  buttonText: {
    color: "white",
  },
  blank: {
    flex: 1,
  },
});
